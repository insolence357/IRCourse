import re
from collections import defaultdict
from text_utils import split_to_sentences

MAX_VALUE = 100000000000

class QueryTerm:
    def __init__(self, term, offset):
        self.offset = offset
        self.term = term
        self.term_len = len(term)

    def __repr__(self):
        return '<%s, %d>' % (self.term, self.offset)

    def __lt__(self, other):
        return self.offset < other.offset


def get_offsets(query_tokens, sentences):
    query_patterns = {query_term: re.compile('(?:^|[^\w])%s(?:[^\w]|$)' % query_term, re.IGNORECASE)
                      for query_term in query_tokens}

    result = []
    for sentence in sentences:
        query_terms = []
        for query_term in query_patterns:
            for match in query_patterns[query_term].finditer(sentence):
                query_terms.append(QueryTerm(query_term, match.start()))
        result.append(query_terms)

    return result


def objective_function(pos, start, end):
    return end-start+1


def cut(pos, max_len):
    n = len(pos)
    end = 0
    curr_sol = 0
    sol_start = 0
    sol_end = 0

    pos.sort()
    for start in range(n):
        for next_end in range(end, n):
            win_len = pos[next_end].offset - pos[start].offset + pos[next_end].term_len
            if win_len > max_len:
                break
            end = next_end
        next_sol = objective_function(pos, start, end)
        if next_sol > curr_sol:
            sol_start = start
            sol_end = end
            curr_sol = next_sol

    return sol_start, sol_end


def aggregate_poses(query_terms):
    q_poses = defaultdict(list)
    for qt in query_terms:
        q_poses[qt.term].append(qt.offset)
    return q_poses

class Snippet:
    def __init__(self, fragment, sent_num, l, r, found_q_infos, query_tokens, sentence_len):
        self.fragment = fragment.strip(' (),.\n')
        self.sent_num = sent_num
        self.l = l
        self.r = r
        self.length = len(self.fragment)
        self.sentence_len = sentence_len
        self.query = query_tokens
        self.query_len = len(self.query)
        self.unique_query_terms = len(set(self.query))
        self.found_q_infos = found_q_infos
        self.unique_found_q = len(self._unique_found_q(found_q_infos))
        self.score = self._calc_score()

    def _unique_found_q(self, query_terms):
        return set([qt.term for qt in query_terms])

    def _calc_score(self):
        q_terms_found_ratio = self.unique_found_q/self.unique_query_terms
        begin_score = 1/(self.l+1)
        final_score = (2/3)*q_terms_found_ratio+(1/5)*begin_score+(2/5)*self.length+(1/6)*(1/(self.sent_num+1))
        return final_score


def get_snippet(query, text, max_len):
    if not text:
        return ''
    min_len = len(query)
    query_tokens = query.split()
    sentences = split_to_sentences(text)
    query_terms = get_offsets(query_tokens, sentences)

    for qt in query_terms:
        qt.sort()

    sent_count = len(sentences)
    candidates = []
    for i in range(sent_count):
        if not query_terms[i]:
            continue
        l, r = cut(query_terms[i], max_len)
        #print(l, r)
        #print(query_terms[i])
        new_l = query_terms[i][l].offset
        new_r = query_terms[i][r].offset+query_terms[i][r].term_len
        fragment = sentences[i][new_l:new_r+1]
        #print(fragment)
        candidates.append(Snippet(fragment, i, new_l, new_r,
                                  query_terms[i][l:r+1], query_tokens, len(sentences[i])))

    candidates.sort(key=lambda snippet: snippet.score, reverse=True)

    result = []
    forward_sent = 3
    for candidate in candidates:
        if candidate.length >= min_len:
            snippet = prolongate(candidate,
                                 ' '.join(sentences[candidate.sent_num:candidate.sent_num+forward_sent]), max_len)
            #print('!', snippet)
            if len(snippet) >= 0.6*max_len:
                result.append((candidate.score, snippet))

    if result:
        result.sort(reverse=True)
        return highlight(query_tokens, result[0][1])
    else:
        return highlight(query_tokens, text[:max_len-3]+'...')


def highlight(query_tokens, snippet):
    #print('highlighting', query_tokens, snippet)
    for token in query_tokens:
        regex = '(?:[^\w]|^)(%s)(?:[^\w]|$)' % token
        #snippet = re.sub(regex, '<b>' +token+ '</b>', snippet, flags=re.IGNORECASE)
        snippet = re.sub(regex, r" <b>\1</b> ", snippet, flags=re.IGNORECASE)
    return snippet


def prolongate(candidate, sentence, max_len):
    result = candidate.fragment + sentence[candidate.r+1:]
    if candidate.l == 0:
        return result[:max_len-3]+'...'
    else:
        return '...'+result[:max_len-6]+'...'


def main():
    #from text import movie_sample, movie_sample2
    #text = movie_sample['description']
    #query = 'джон и но'
    #query = 'грегори джон джеймс'
    #query = 'и но'
    #query = 'джеймс лето'
    #text = movie_sample2['description']
    #query = 'дэвид лучше прежнего и но'
    #print(get_snippet(query, text, 180))
    pass

if __name__ == '__main__':
    main()
